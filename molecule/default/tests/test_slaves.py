import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ns_slaves')


def test_zone_axfr(host, local_config):
    master_ip = local_config['ics-ans-role-powerdns-default-master']['ansible_facts']['ansible_default_ipv4']['address']
    cmd = host.run("host -t axfr cslab.esss.lu.se %s" % master_ip)
    assert cmd.rc == 0
