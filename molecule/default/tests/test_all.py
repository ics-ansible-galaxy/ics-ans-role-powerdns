import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.first
def test_gather_config(host, local_config):
    ansible_setup = host.ansible("setup")
    res = host.run("hostname")
    hostname = res.stdout.strip()
    local_config[hostname] = ansible_setup
