import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('recursors')


def test_pdns_port(host):
    # print(host.ansible.get_variables())
    # localip = host.ansible.get_variables()['ansible_default_ipv4']['address']
    assert host.socket("udp://0.0.0.0:53").is_listening


def test_google(host):
    cmd = host.run("host www.google.it 127.0.0.1")
    assert cmd.rc == 0


def test_direct(host):
    cmd = host.run("host test2.tn.esss.lu.se 127.0.0.1")
    assert cmd.rc == 0


def test_reverse(host):
    cmd = host.run("host 10.65.33.84 127.0.0.1")
    assert cmd.rc == 0
