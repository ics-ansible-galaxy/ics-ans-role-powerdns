import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ns_slaves')


def test_zone_axfr(host):
    cmd = host.run("host -t axfr tn.esss.lu.se 172.17.0.2")
    assert cmd.rc == 0
