import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('recursors')


def test_pdns_port(host):
    assert host.socket("udp://0.0.0.0:53").is_listening


def test_present(host):
    hosts = ["www.google.it",
             "ztp.tn.esss.lu.se",
             "172.16.2.5",
             "alarm-lcr.tn.esss.lu.se"]
    for one_host in hosts:
        cmd = host.run("host {} 127.0.0.1".format(one_host))
        assert cmd.rc == 0


def test_not_present(host):
    hosts = ["wrong.tn.esss.lu.se",
             "wrong2.tn.esss.lu.se",
             "wrong3.tn.esss.lu.se",
             "172.16.2.241",
             "172.16.2.123",
             "172.16.2.124",
             "172.16.107.113",
             "zpp.tn.esss.lu.se"]
    for one_host in hosts:
        cmd = host.run("host {} 127.0.0.1".format(one_host))
        assert cmd.rc == 1


def test_match(host):
    hosts = [{"fqdn": "ztp.tn.esss.lu.se", "res": "172.16.2.5"},
             {"fqdn": "alarm-lcr.tn.esss.lu.se", "res": "alarm-01"}]
    for one_host in hosts:
        cmd = host.run("host {} 127.0.0.1".format(one_host['fqdn']))
        assert one_host['res'] in cmd.stdout
