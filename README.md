ics-ans-role-powerdns
===================

Ansible role to install powerdns.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-powerdns
```

License
-------

BSD 2-clause
