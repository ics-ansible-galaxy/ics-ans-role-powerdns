# python 3 headers, required if submitting to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import os
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

DOCUMENTATION = """
    lookup: csentry
    author:
      - Benjamin Bertrand
    short_description: fetch data from CSEntry
    description:
      - This lookup returns data from CSEntry
    requirements:
      - csentry-api (https://gitlab.esss.lu.se/ics-infrastructure/csentry-api.git)
    options:
      _terms:
        description: list of keys to query
        required: True
      url:
        description: URL of the CSEntry web server
        default: 'https://csentry.esss.lu.se'
        env:
          - name: ANSIBLE_CSENTRY_URL
      token:
        description: the token to access the CSEntry API
        env:
          - name: ANSIBLE_CSENTRY_TOKEN
"""

RETURN = """
_raw:
  description: value(s) stored in CSEntry
"""

HAVE_CSENTRY = False
try:
    from csentry import CSEntry
    HAVE_CSENTRY = True
except ImportError:
    pass


class LookupModule(LookupBase):

    def run(self, terms, variables=None, **kwargs):
        if not HAVE_CSENTRY:
            raise AnsibleError("Can't LOOKUP(csentry): module csentry-api is not installed")

        display.v('kwargs: %s' % kwargs)
        # get options
        url = os.environ.get('ANSIBLE_CSENTRY_URL', 'https://csentry.esss.lu.se')
        token = os.environ.get('ANSIBLE_CSENTRY_TOKEN', None)
        url = kwargs.pop('url', url)
        token = kwargs.pop('token', token)
        csentry = CSEntry(token=token, url=url)
        display.v(url)
        display.v(token)
        display.v('kwargs: %s' % kwargs)
        # lookups in general are expected to both take a list as input and output a list
        # this is done so they work with the looping construct `with_`.
        ret = []
        for term in terms:
            display.v("File lookup term: %s" % term)

            try:
                function = getattr(csentry, 'get_{}'.format(term))
            except AttributeError:
                raise AnsibleError("Invalide option '{}'".format(term))
            try:
                res = function(**kwargs)
                if res is None:
                    res = ""
                ret.extend(list(res))
            except Exception:
                ret.append("")  # connection failed or key not found

        return ret
