class FilterModule(object):
    def filters(self):
        return {'reverse_zones': self.reverse_zones,
                'extract_ip': self.extract_ip,
                'rrset_to_hostmap': self.rrset_to_hostmap,
                'rrset_to_cnames': self.rrset_to_cnames,
                'hostmap_filter': self.hostmap_filter,
                'cnames_filter': self.cnames_filter,
                'reverse_zone': self.reverse_zone,
                'csentry_inventory_to_pdns': self.csentry_inventory_to_pdns
                }

    def csentry_inventory_to_pdns(self, my_hostvars, filter_by_domain=None, get_cnames=False):
        ret = []
        for hostvar in my_hostvars.values():
            for interface in hostvar.get("csentry_interfaces", []):
                if filter_by_domain is not None and interface.get("network", {}).get("domain", None) != filter_by_domain:
                    continue
                if not get_cnames:
                    ip = interface.get("ip", None)
                    hostname = interface.get("name", None)
                    if ip is not None and hostname is not None:
                        ret.append({'ip': ip, 'name': hostname})
                else:
                    iface = interface.get("name", None)
                    cnames = interface.get("cnames", [])
                    if iface is not None and isinstance(cnames, list):
                        for cname in cnames:
                            ret.append({'interface': iface, 'name': cname})
        return ret

    def reverse_zone(self, ip):
        ip_split = ip.split(".")
        if len(ip_split) == 4:
            return("%s.%s.%s.in-addr.arpa." % (ip_split[2], ip_split[1], ip_split[0]))
        else:
            return None

    def reverse_zones(self, iplist):
        ret = []
        for ip in iplist:
            result = self.reverse_zone(ip)
            if result is not None:
                ret.append(result)
        return ret

    def extract_ip(self, hostmap):
        ret = []
        for entry in hostmap:
            if type(entry) is dict and 'ip' in entry.keys():
                ret.append(entry['ip'])
        return ret

    def rrset_to_hostmap(self, rrsets):
        ret = {}
        for item in rrsets:
            if item['type'] == "A" and 'records' in item and not item['records'][0]['disabled']:
                ip = item['records'][0]['content']
                name = item['name'].split('.')[0]
                ret[name] = ip
        return ret

    def rrset_to_cnames(self, rrsets):
        ret = {}
        for item in rrsets:
            if item['type'] == "CNAME" and 'records' in item and not item['records'][0]['disabled']:
                target = item['records'][0]['content'].split('.')[0]
                name = item['name'].split('.')[0]
                ret[name] = target
        return ret

    def hostmap_filter(self, hostmap, current_ip_index, dns_servers):
        ret = dict(current_ip_index)
        for item in hostmap:
            ret.pop(item['name'], None)
        for item in dns_servers:
            ret.pop(item['name'], None)
        return ret

    def cnames_filter(self, cnames, current_cnames_index):
        ret = dict(current_cnames_index)
        for item in cnames:
            ret.pop(item['name'], None)
        return ret
